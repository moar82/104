/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.net.udp.mc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.net.udp.mc.MCGroupAdapter;
import com.aelitis.net.udp.mc.MCGroupException;
import com.aelitis.net.udp.mc.MCGroupFactory;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class MCGroupFactoryEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      String[] stringArray0 = new String[2];
      try {
        MCGroupFactory.getSingleton((MCGroupAdapter) null, "", 16777266, (-1577), stringArray0);
        fail("Expecting exception: MCGroupException");
      } catch(MCGroupException e) {
        /*
         * Failed to initialise MCGroup
         */
      }
  }

  @Test
  public void test1()  throws Throwable  {
      MCGroupFactory mCGroupFactory0 = new MCGroupFactory();
      assertNotNull(mCGroupFactory0);
  }
}
