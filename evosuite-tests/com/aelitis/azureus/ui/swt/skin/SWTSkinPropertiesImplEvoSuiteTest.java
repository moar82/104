/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.ui.swt.skin;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.ui.swt.skin.SWTSkinProperties;
import com.aelitis.azureus.ui.swt.skin.SWTSkinPropertiesClone;
import com.aelitis.azureus.ui.swt.skin.SWTSkinPropertiesImpl;
import org.eclipse.swt.graphics.Color;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class SWTSkinPropertiesImplEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      SWTSkinPropertiesImpl sWTSkinPropertiesImpl0 = new SWTSkinPropertiesImpl();
      String[] stringArray0 = new String[9];
      stringArray0[0] = "[zjJebYwXAp#S4";
      SWTSkinPropertiesClone sWTSkinPropertiesClone0 = new SWTSkinPropertiesClone((SWTSkinProperties) sWTSkinPropertiesImpl0, "[zjJebYwXAp#S4", stringArray0);
      Color color0 = sWTSkinPropertiesClone0.getColor("[zjJebYwXAp#S4");
      assertNull(color0);
  }

  @Test
  public void test1()  throws Throwable  {
      SWTSkinPropertiesImpl sWTSkinPropertiesImpl0 = new SWTSkinPropertiesImpl();
      sWTSkinPropertiesImpl0.clearCache();
  }

  @Test
  public void test2()  throws Throwable  {
      SWTSkinPropertiesImpl sWTSkinPropertiesImpl0 = new SWTSkinPropertiesImpl();
      Color color0 = sWTSkinPropertiesImpl0.getColor("[zjJebYwXAp#S4", (Color) null);
      assertNull(color0);
  }
}
