/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.ui.common;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.ui.common.RememberedDecisionsManager;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.evosuite.Properties.SandboxMode;
import org.evosuite.sandbox.Sandbox;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class RememberedDecisionsManagerEvoSuiteTest {

  private static ExecutorService executor; 

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
    org.evosuite.Properties.SANDBOX_MODE = SandboxMode.RECOMMENDED; 
    Sandbox.initializeSecurityManagerForSUT(); 
    executor = Executors.newCachedThreadPool(); 
  } 

  @AfterClass 
  public static void clearEvoSuiteFramework(){ 
    executor.shutdownNow(); 
    Sandbox.resetDefaultSecurityManager(); 
  } 

  @Before 
  public void initTestCase(){ 
    Sandbox.goingToExecuteSUTCode(); 
  } 

  @After 
  public void doneWithTestCase(){ 
    Sandbox.doneWithExecutingSUTCode(); 
  } 


  @Test
  public void test0()  throws Throwable  {
      RememberedDecisionsManager rememberedDecisionsManager0 = new RememberedDecisionsManager();
      assertNotNull(rememberedDecisionsManager0);
  }

  @Test
  public void test1()  throws Throwable  {
      int int0 = RememberedDecisionsManager.getRememberedDecision("");
      assertEquals((-1), int0);
  }

  @Test
  public void test2()  throws Throwable  {
      int int0 = RememberedDecisionsManager.getRememberedDecision((String) null);
      assertEquals((-1), int0);
  }

  @Test
  public void test3()  throws Throwable  {
      int int0 = RememberedDecisionsManager.getRememberedDecision("", 0);
      assertEquals((-1), int0);
  }

  @Test
  public void test4()  throws Throwable  {
      int int0 = RememberedDecisionsManager.getRememberedDecision("jW-GlI");
      assertEquals((-1), int0);
  }

  @Test
  public void test5()  throws Throwable  {
      int int0 = RememberedDecisionsManager.getRememberedDecision("", (-1667));
      assertEquals((-1), int0);
  }

  @Test
  public void test6()  throws Throwable  {
    Future<?> future = executor.submit(new Runnable(){ 
            public void run() { 
          // Undeclared exception!
          try {
            RememberedDecisionsManager.setRemembered("", 1134);
            fail("Expecting exception: SecurityException");
          } catch(SecurityException e) {
            /*
             * Security manager blocks (java.io.FilePermission /home/ac1gf/.azureus/azureus.config.bak delete)
             * java.lang.Thread.getStackTrace(Thread.java:1479)
             * org.evosuite.sandbox.MSecurityManager.checkPermission(MSecurityManager.java:303)
             * java.lang.SecurityManager.checkDelete(SecurityManager.java:990)
             * java.io.File.delete(File.java:902)
             * org.gudy.azureus2.core3.util.FileUtil.backupFile(FileUtil.java:999)
             * org.gudy.azureus2.core3.util.FileUtil.writeResilientFile(FileUtil.java:462)
             * org.gudy.azureus2.core3.util.FileUtil.writeResilientFile(FileUtil.java:445)
             * org.gudy.azureus2.core3.util.FileUtil.writeResilientConfigFile(FileUtil.java:418)
             * org.gudy.azureus2.core3.config.impl.ConfigurationManager.save(ConfigurationManager.java:249)
             * org.gudy.azureus2.core3.config.impl.ConfigurationManager.save(ConfigurationManager.java:279)
             * org.gudy.azureus2.core3.config.COConfigurationManager.save(COConfigurationManager.java:485)
             * com.aelitis.azureus.ui.common.RememberedDecisionsManager.setRemembered(RememberedDecisionsManager.java:87)
             * sun.reflect.GeneratedMethodAccessor9.invoke(Unknown Source)
             * sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
             * java.lang.reflect.Method.invoke(Method.java:597)
             * org.evosuite.testcase.MethodStatement$1.execute(MethodStatement.java:262)
             * org.evosuite.testcase.AbstractStatement.exceptionHandler(AbstractStatement.java:142)
             * org.evosuite.testcase.MethodStatement.execute(MethodStatement.java:217)
             * org.evosuite.testcase.TestRunnable.call(TestRunnable.java:291)
             * org.evosuite.testcase.TestRunnable.call(TestRunnable.java:44)
             * java.util.concurrent.FutureTask$Sync.innerRun(FutureTask.java:303)
             * java.util.concurrent.FutureTask.run(FutureTask.java:138)
             * java.util.concurrent.ThreadPoolExecutor$Worker.runTask(ThreadPoolExecutor.java:886)
             * java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:908)
             * java.lang.Thread.run(Thread.java:662)
             */
          }
      } 
    }); 
    future.get(6000, TimeUnit.MILLISECONDS); 
  }

  @Test
  public void test7()  throws Throwable  {
      RememberedDecisionsManager.setRemembered((String) null, 0);
  }

  @Test
  public void test8()  throws Throwable  {
    Future<?> future = executor.submit(new Runnable(){ 
            public void run() { 
          // Undeclared exception!
          try {
            RememberedDecisionsManager.setRemembered("jW-GlI", (-1));
            fail("Expecting exception: SecurityException");
          } catch(SecurityException e) {
            /*
             * Security manager blocks (java.io.FilePermission /home/ac1gf/.azureus/azureus.config.bak delete)
             * java.lang.Thread.getStackTrace(Thread.java:1479)
             * org.evosuite.sandbox.MSecurityManager.checkPermission(MSecurityManager.java:303)
             * java.lang.SecurityManager.checkDelete(SecurityManager.java:990)
             * java.io.File.delete(File.java:902)
             * org.gudy.azureus2.core3.util.FileUtil.backupFile(FileUtil.java:999)
             * org.gudy.azureus2.core3.util.FileUtil.writeResilientFile(FileUtil.java:462)
             * org.gudy.azureus2.core3.util.FileUtil.writeResilientFile(FileUtil.java:445)
             * org.gudy.azureus2.core3.util.FileUtil.writeResilientConfigFile(FileUtil.java:418)
             * org.gudy.azureus2.core3.config.impl.ConfigurationManager.save(ConfigurationManager.java:249)
             * org.gudy.azureus2.core3.config.impl.ConfigurationManager.save(ConfigurationManager.java:279)
             * org.gudy.azureus2.core3.config.COConfigurationManager.save(COConfigurationManager.java:485)
             * com.aelitis.azureus.ui.common.RememberedDecisionsManager.setRemembered(RememberedDecisionsManager.java:87)
             * sun.reflect.GeneratedMethodAccessor9.invoke(Unknown Source)
             * sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
             * java.lang.reflect.Method.invoke(Method.java:597)
             * org.evosuite.testcase.MethodStatement$1.execute(MethodStatement.java:262)
             * org.evosuite.testcase.AbstractStatement.exceptionHandler(AbstractStatement.java:142)
             * org.evosuite.testcase.MethodStatement.execute(MethodStatement.java:217)
             * org.evosuite.testcase.TestRunnable.call(TestRunnable.java:291)
             * org.evosuite.testcase.TestRunnable.call(TestRunnable.java:44)
             * java.util.concurrent.FutureTask$Sync.innerRun(FutureTask.java:303)
             * java.util.concurrent.FutureTask.run(FutureTask.java:138)
             * java.util.concurrent.ThreadPoolExecutor$Worker.runTask(ThreadPoolExecutor.java:886)
             * java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:908)
             * java.lang.Thread.run(Thread.java:662)
             */
          }
      } 
    }); 
    future.get(6000, TimeUnit.MILLISECONDS); 
  }
}
