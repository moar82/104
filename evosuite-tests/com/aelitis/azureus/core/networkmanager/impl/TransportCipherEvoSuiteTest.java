/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.networkmanager.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.networkmanager.impl.TransportCipher;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.MGF1ParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class TransportCipherEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      byte[] byteArray0 = new byte[5];
      SecretKeySpec secretKeySpec0 = new SecretKeySpec(byteArray0, "RC4");
      TransportCipher transportCipher0 = new TransportCipher("RC4", 1, secretKeySpec0);
      assertNotNull(transportCipher0);
      
      byte[] byteArray1 = transportCipher0.update(byteArray0, (int) (byte)0, 0);
      assertNotSame(byteArray1, byteArray0);
      assertNotNull(byteArray1);
  }

  @Test
  public void test1()  throws Throwable  {
      byte[] byteArray0 = new byte[12];
      SecretKeySpec secretKeySpec0 = new SecretKeySpec(byteArray0, 5, 5, "]wtZR!IT5pf");
      MGF1ParameterSpec mGF1ParameterSpec0 = MGF1ParameterSpec.SHA256;
      TransportCipher transportCipher0 = null;
      try {
        transportCipher0 = new TransportCipher("]wtZR!IT5pf", 5, secretKeySpec0, (AlgorithmParameterSpec) mGF1ParameterSpec0);
        fail("Expecting exception: NoSuchAlgorithmException");
      } catch(NoSuchAlgorithmException e) {
        /*
         * Cannot find any provider supporting ]wtZR!IT5pf
         */
      }
  }

  @Test
  public void test2()  throws Throwable  {
      TransportCipher transportCipher0 = null;
      try {
        transportCipher0 = new TransportCipher("S", 72, (SecretKeySpec) null);
        fail("Expecting exception: NoSuchAlgorithmException");
      } catch(NoSuchAlgorithmException e) {
        /*
         * Cannot find any provider supporting S
         */
      }
  }

  @Test
  public void test3()  throws Throwable  {
      byte[] byteArray0 = new byte[5];
      SecretKeySpec secretKeySpec0 = new SecretKeySpec(byteArray0, "RC4");
      TransportCipher transportCipher0 = new TransportCipher("RC4", 0, secretKeySpec0);
      assertEquals("RC4-160", transportCipher0.getName());
  }

  @Test
  public void test4()  throws Throwable  {
      byte[] byteArray0 = new byte[5];
      SecretKeySpec secretKeySpec0 = new SecretKeySpec(byteArray0, "RC4");
      TransportCipher transportCipher0 = new TransportCipher("RC4", 1, secretKeySpec0);
      assertNotNull(transportCipher0);
      
      String string0 = transportCipher0.getName();
      assertNotNull(string0);
      assertEquals("RC4-160", string0);
  }
}
