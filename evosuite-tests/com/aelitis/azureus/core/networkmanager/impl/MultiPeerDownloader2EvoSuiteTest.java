/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.networkmanager.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.networkmanager.ConnectionEndpoint;
import com.aelitis.azureus.core.networkmanager.EventWaiter;
import com.aelitis.azureus.core.networkmanager.NetworkConnectionBase;
import com.aelitis.azureus.core.networkmanager.RateHandler;
import com.aelitis.azureus.core.networkmanager.impl.MultiPeerDownloader2;
import com.aelitis.azureus.core.networkmanager.impl.NetworkConnectionImpl;
import com.aelitis.azureus.core.peermanager.messaging.MessageStreamDecoder;
import com.aelitis.azureus.core.peermanager.messaging.MessageStreamEncoder;
import com.aelitis.azureus.core.peermanager.messaging.azureus.AZMessageDecoder;
import com.aelitis.azureus.core.peermanager.messaging.bittorrent.BTMessageEncoder;
import java.net.InetSocketAddress;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class MultiPeerDownloader2EvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      MultiPeerDownloader2.connectionList multiPeerDownloader2_connectionList0 = new MultiPeerDownloader2.connectionList();
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry0 = multiPeerDownloader2_connectionList0.head();
      assertNull(multiPeerDownloader2_connectionEntry0);
  }

  @Test
  public void test1()  throws Throwable  {
      MultiPeerDownloader2 multiPeerDownloader2_0 = new MultiPeerDownloader2((RateHandler) null);
      long long0 = multiPeerDownloader2_0.getBytesReadyToWrite();
      assertEquals(0L, long0);
  }

  @Test
  public void test2()  throws Throwable  {
      MultiPeerDownloader2 multiPeerDownloader2_0 = new MultiPeerDownloader2((RateHandler) null);
      int int0 = multiPeerDownloader2_0.getPriority();
      assertEquals(1, int0);
  }

  @Test
  public void test3()  throws Throwable  {
      MultiPeerDownloader2 multiPeerDownloader2_0 = new MultiPeerDownloader2((RateHandler) null);
      boolean boolean0 = multiPeerDownloader2_0.getPriorityBoost();
      assertEquals(false, boolean0);
  }

  @Test
  public void test4()  throws Throwable  {
      MultiPeerDownloader2 multiPeerDownloader2_0 = new MultiPeerDownloader2((RateHandler) null);
      RateHandler rateHandler0 = multiPeerDownloader2_0.getRateHandler();
      assertNull(rateHandler0);
  }

  @Test
  public void test5()  throws Throwable  {
      MultiPeerDownloader2 multiPeerDownloader2_0 = new MultiPeerDownloader2((RateHandler) null);
      int int0 = multiPeerDownloader2_0.getConnectionCount();
      assertEquals(0, int0);
  }

  @Test
  public void test6()  throws Throwable  {
      MultiPeerDownloader2 multiPeerDownloader2_0 = new MultiPeerDownloader2((RateHandler) null);
      ConnectionEndpoint connectionEndpoint0 = new ConnectionEndpoint((InetSocketAddress) null);
      AZMessageDecoder aZMessageDecoder0 = new AZMessageDecoder();
      byte[][] byteArray0 = new byte[1][7];
      NetworkConnectionImpl networkConnectionImpl0 = new NetworkConnectionImpl(connectionEndpoint0, (MessageStreamEncoder) null, (MessageStreamDecoder) aZMessageDecoder0, false, true, byteArray0);
      multiPeerDownloader2_0.addPeerConnection((NetworkConnectionBase) networkConnectionImpl0);
      multiPeerDownloader2_0.addPeerConnection((NetworkConnectionBase) networkConnectionImpl0);
      String string0 = multiPeerDownloader2_0.getString();
      assertEquals(2, multiPeerDownloader2_0.getConnectionCount());
      assertEquals("MPD (2/0/0: tran=null,in=-1,out=0,owner=null,tran=null,in=-1,out=0,owner=null", string0);
  }

  @Test
  public void test7()  throws Throwable  {
      MultiPeerDownloader2 multiPeerDownloader2_0 = new MultiPeerDownloader2((RateHandler) null);
      ConnectionEndpoint connectionEndpoint0 = new ConnectionEndpoint((InetSocketAddress) null);
      AZMessageDecoder aZMessageDecoder0 = new AZMessageDecoder();
      byte[][] byteArray0 = new byte[1][7];
      NetworkConnectionImpl networkConnectionImpl0 = new NetworkConnectionImpl(connectionEndpoint0, (MessageStreamEncoder) null, (MessageStreamDecoder) aZMessageDecoder0, false, true, byteArray0);
      multiPeerDownloader2_0.addPeerConnection((NetworkConnectionBase) networkConnectionImpl0);
      assertEquals("MPD (1/0/0: tran=null,in=-1,out=0,owner=null", multiPeerDownloader2_0.getString());
      
      boolean boolean0 = multiPeerDownloader2_0.removePeerConnection((NetworkConnectionBase) networkConnectionImpl0);
      assertEquals(true, boolean0);
  }

  @Test
  public void test8()  throws Throwable  {
      MultiPeerDownloader2 multiPeerDownloader2_0 = new MultiPeerDownloader2((RateHandler) null);
      boolean boolean0 = multiPeerDownloader2_0.removePeerConnection((NetworkConnectionBase) null);
      assertEquals(false, boolean0);
  }

  @Test
  public void test9()  throws Throwable  {
      MultiPeerDownloader2 multiPeerDownloader2_0 = new MultiPeerDownloader2((RateHandler) null);
      EventWaiter eventWaiter0 = new EventWaiter();
      int int0 = multiPeerDownloader2_0.getReadyConnectionCount(eventWaiter0);
      assertEquals(0, int0);
  }

  @Test
  public void test10()  throws Throwable  {
      MultiPeerDownloader2 multiPeerDownloader2_0 = new MultiPeerDownloader2((RateHandler) null);
      EventWaiter eventWaiter0 = new EventWaiter();
      multiPeerDownloader2_0.addPeerConnection((NetworkConnectionBase) null);
      // Undeclared exception!
      try {
        multiPeerDownloader2_0.getReadyConnectionCount(eventWaiter0);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test11()  throws Throwable  {
      MultiPeerDownloader2 multiPeerDownloader2_0 = new MultiPeerDownloader2((RateHandler) null);
      ConnectionEndpoint connectionEndpoint0 = new ConnectionEndpoint((InetSocketAddress) null);
      AZMessageDecoder aZMessageDecoder0 = new AZMessageDecoder();
      byte[][] byteArray0 = new byte[1][7];
      NetworkConnectionImpl networkConnectionImpl0 = new NetworkConnectionImpl(connectionEndpoint0, (MessageStreamEncoder) null, (MessageStreamDecoder) aZMessageDecoder0, false, true, byteArray0);
      multiPeerDownloader2_0.addPeerConnection((NetworkConnectionBase) networkConnectionImpl0);
      String string0 = multiPeerDownloader2_0.getString();
      assertEquals(1, multiPeerDownloader2_0.getConnectionCount());
      assertEquals("MPD (1/0/0: tran=null,in=-1,out=0,owner=null", string0);
  }

  @Test
  public void test12()  throws Throwable  {
      MultiPeerDownloader2.connectionList multiPeerDownloader2_connectionList0 = new MultiPeerDownloader2.connectionList();
      BTMessageEncoder bTMessageEncoder0 = new BTMessageEncoder();
      AZMessageDecoder aZMessageDecoder0 = new AZMessageDecoder();
      byte[][] byteArray0 = new byte[7][7];
      NetworkConnectionImpl networkConnectionImpl0 = new NetworkConnectionImpl((ConnectionEndpoint) null, (MessageStreamEncoder) bTMessageEncoder0, (MessageStreamDecoder) aZMessageDecoder0, true, true, byteArray0);
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry0 = new MultiPeerDownloader2.connectionEntry((NetworkConnectionBase) networkConnectionImpl0);
      multiPeerDownloader2_connectionList0.addToStart(multiPeerDownloader2_connectionEntry0);
      multiPeerDownloader2_connectionList0.add((NetworkConnectionBase) null);
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry1 = multiPeerDownloader2_connectionList0.remove((NetworkConnectionBase) null);
      // Undeclared exception!
      try {
        multiPeerDownloader2_connectionList0.moveToEnd(multiPeerDownloader2_connectionEntry1);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test13()  throws Throwable  {
      MultiPeerDownloader2.connectionList multiPeerDownloader2_connectionList0 = new MultiPeerDownloader2.connectionList();
      multiPeerDownloader2_connectionList0.add((NetworkConnectionBase) null);
      assertEquals(1, multiPeerDownloader2_connectionList0.size());
  }

  @Test
  public void test14()  throws Throwable  {
      MultiPeerDownloader2.connectionList multiPeerDownloader2_connectionList0 = new MultiPeerDownloader2.connectionList();
      BTMessageEncoder bTMessageEncoder0 = new BTMessageEncoder();
      AZMessageDecoder aZMessageDecoder0 = new AZMessageDecoder();
      byte[][] byteArray0 = new byte[7][7];
      NetworkConnectionImpl networkConnectionImpl0 = new NetworkConnectionImpl((ConnectionEndpoint) null, (MessageStreamEncoder) bTMessageEncoder0, (MessageStreamDecoder) aZMessageDecoder0, true, true, byteArray0);
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry0 = new MultiPeerDownloader2.connectionEntry((NetworkConnectionBase) networkConnectionImpl0);
      multiPeerDownloader2_connectionList0.addToStart(multiPeerDownloader2_connectionEntry0);
      multiPeerDownloader2_connectionList0.addToEnd(multiPeerDownloader2_connectionEntry0);
      assertEquals(2, multiPeerDownloader2_connectionList0.size());
  }

  @Test
  public void test15()  throws Throwable  {
      MultiPeerDownloader2.connectionList multiPeerDownloader2_connectionList0 = new MultiPeerDownloader2.connectionList();
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry0 = new MultiPeerDownloader2.connectionEntry((NetworkConnectionBase) null);
      multiPeerDownloader2_connectionList0.addToEnd(multiPeerDownloader2_connectionEntry0);
      assertEquals(1, multiPeerDownloader2_connectionList0.size());
  }

  @Test
  public void test16()  throws Throwable  {
      MultiPeerDownloader2.connectionList multiPeerDownloader2_connectionList0 = new MultiPeerDownloader2.connectionList();
      BTMessageEncoder bTMessageEncoder0 = new BTMessageEncoder();
      AZMessageDecoder aZMessageDecoder0 = new AZMessageDecoder();
      byte[][] byteArray0 = new byte[7][7];
      NetworkConnectionImpl networkConnectionImpl0 = new NetworkConnectionImpl((ConnectionEndpoint) null, (MessageStreamEncoder) bTMessageEncoder0, (MessageStreamDecoder) aZMessageDecoder0, true, true, byteArray0);
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry0 = new MultiPeerDownloader2.connectionEntry((NetworkConnectionBase) networkConnectionImpl0);
      multiPeerDownloader2_connectionList0.addToStart(multiPeerDownloader2_connectionEntry0);
      multiPeerDownloader2_connectionList0.moveToEnd(multiPeerDownloader2_connectionEntry0);
      assertEquals(1, multiPeerDownloader2_connectionList0.size());
  }

  @Test
  public void test17()  throws Throwable  {
      MultiPeerDownloader2.connectionList multiPeerDownloader2_connectionList0 = new MultiPeerDownloader2.connectionList();
      BTMessageEncoder bTMessageEncoder0 = new BTMessageEncoder();
      AZMessageDecoder aZMessageDecoder0 = new AZMessageDecoder();
      byte[][] byteArray0 = new byte[7][7];
      NetworkConnectionImpl networkConnectionImpl0 = new NetworkConnectionImpl((ConnectionEndpoint) null, (MessageStreamEncoder) bTMessageEncoder0, (MessageStreamDecoder) aZMessageDecoder0, true, true, byteArray0);
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry0 = new MultiPeerDownloader2.connectionEntry((NetworkConnectionBase) networkConnectionImpl0);
      multiPeerDownloader2_connectionList0.addToStart(multiPeerDownloader2_connectionEntry0);
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry1 = multiPeerDownloader2_connectionList0.remove((NetworkConnectionBase) null);
      // Undeclared exception!
      try {
        multiPeerDownloader2_connectionList0.moveToEnd(multiPeerDownloader2_connectionEntry1);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test18()  throws Throwable  {
      MultiPeerDownloader2.connectionList multiPeerDownloader2_connectionList0 = new MultiPeerDownloader2.connectionList();
      BTMessageEncoder bTMessageEncoder0 = new BTMessageEncoder();
      AZMessageDecoder aZMessageDecoder0 = new AZMessageDecoder();
      byte[][] byteArray0 = new byte[7][7];
      NetworkConnectionImpl networkConnectionImpl0 = new NetworkConnectionImpl((ConnectionEndpoint) null, (MessageStreamEncoder) bTMessageEncoder0, (MessageStreamDecoder) aZMessageDecoder0, true, true, byteArray0);
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry0 = new MultiPeerDownloader2.connectionEntry((NetworkConnectionBase) networkConnectionImpl0);
      // Undeclared exception!
      try {
        multiPeerDownloader2_connectionList0.moveToEnd(multiPeerDownloader2_connectionEntry0);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test19()  throws Throwable  {
      MultiPeerDownloader2.connectionList multiPeerDownloader2_connectionList0 = new MultiPeerDownloader2.connectionList();
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry0 = new MultiPeerDownloader2.connectionEntry((NetworkConnectionBase) null);
      multiPeerDownloader2_connectionList0.addToStart(multiPeerDownloader2_connectionEntry0);
      assertEquals(1, multiPeerDownloader2_connectionList0.size());
      
      multiPeerDownloader2_connectionList0.remove((NetworkConnectionBase) null);
      assertEquals(0, multiPeerDownloader2_connectionList0.size());
  }

  @Test
  public void test20()  throws Throwable  {
      MultiPeerDownloader2.connectionList multiPeerDownloader2_connectionList0 = new MultiPeerDownloader2.connectionList();
      BTMessageEncoder bTMessageEncoder0 = new BTMessageEncoder();
      AZMessageDecoder aZMessageDecoder0 = new AZMessageDecoder();
      byte[][] byteArray0 = new byte[7][7];
      NetworkConnectionImpl networkConnectionImpl0 = new NetworkConnectionImpl((ConnectionEndpoint) null, (MessageStreamEncoder) bTMessageEncoder0, (MessageStreamDecoder) aZMessageDecoder0, true, true, byteArray0);
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry0 = new MultiPeerDownloader2.connectionEntry((NetworkConnectionBase) networkConnectionImpl0);
      multiPeerDownloader2_connectionList0.addToStart(multiPeerDownloader2_connectionEntry0);
      MultiPeerDownloader2.connectionEntry multiPeerDownloader2_connectionEntry1 = new MultiPeerDownloader2.connectionEntry((NetworkConnectionBase) null);
      multiPeerDownloader2_connectionList0.addToStart(multiPeerDownloader2_connectionEntry1);
      multiPeerDownloader2_connectionList0.remove((NetworkConnectionBase) null);
      assertEquals(1, multiPeerDownloader2_connectionList0.size());
  }
}
