/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.networkmanager.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.networkmanager.impl.ProtocolDecoder;
import com.aelitis.azureus.core.networkmanager.impl.ProtocolDecoderAdapter;
import com.aelitis.azureus.core.networkmanager.impl.ProtocolDecoderInitial;
import com.aelitis.azureus.core.networkmanager.impl.TransportHelper;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class ProtocolDecoderEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      byte[][] byteArray0 = new byte[3][5];
      ProtocolDecoder.addSecrets(byteArray0);
  }

  @Test
  public void test1()  throws Throwable  {
      byte[][] byteArray0 = new byte[3][5];
      ProtocolDecoder.removeSecrets(byteArray0);
  }

  @Test
  public void test2()  throws Throwable  {
      byte[][] byteArray0 = new byte[3][5];
      ProtocolDecoderInitial protocolDecoderInitial0 = null;
      try {
        protocolDecoderInitial0 = new ProtocolDecoderInitial((TransportHelper) null, byteArray0, true, (ByteBuffer) null, (ProtocolDecoderAdapter) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }
}
