/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.networkmanager.impl.http;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.networkmanager.NetworkConnection;
import com.aelitis.azureus.core.networkmanager.impl.TransportHelper;
import com.aelitis.azureus.core.networkmanager.impl.http.HTTPNetworkManager;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class HTTPNetworkManagerEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      HTTPNetworkManager hTTPNetworkManager0 = HTTPNetworkManager.getSingleton();
      String string0 = hTTPNetworkManager0.getRangeNotSatisfiable();
      assertNotNull(string0);
      assertEquals("HTTP/1.1 416 Not Satisfiable\r\nConnection: Close\r\nContent-Length: 0\r\n\r\n", string0);
  }

  @Test
  public void test1()  throws Throwable  {
      HTTPNetworkManager hTTPNetworkManager0 = HTTPNetworkManager.getSingleton();
      String string0 = hTTPNetworkManager0.getIndexPage();
      assertEquals("HTTP/1.1 200 OK\r\nConnection: Close\r\nContent-Length: 0\r\n\r\n", string0);
      assertNotNull(string0);
  }

  @Test
  public void test2()  throws Throwable  {
      HTTPNetworkManager hTTPNetworkManager0 = HTTPNetworkManager.getSingleton();
      hTTPNetworkManager0.addURLHandler((HTTPNetworkManager.URLHandler) null);
      assertEquals("HTTP/1.1 404 Not Found\r\nConnection: Close\r\nContent-Length: 0\r\n\r\n", hTTPNetworkManager0.getNotFound());
  }

  @Test
  public void test3()  throws Throwable  {
      HTTPNetworkManager hTTPNetworkManager0 = HTTPNetworkManager.getSingleton();
      hTTPNetworkManager0.clearExplicitBindAddress();
      assertEquals(8080, hTTPNetworkManager0.getHTTPListeningPortNumber());
  }

  @Test
  public void test4()  throws Throwable  {
      HTTPNetworkManager hTTPNetworkManager0 = HTTPNetworkManager.getSingleton();
      hTTPNetworkManager0.removeURLHandler((HTTPNetworkManager.URLHandler) null);
      assertEquals("HTTP/1.1 404 Not Found\r\nConnection: Close\r\nContent-Length: 0\r\n\r\n", hTTPNetworkManager0.getNotFound());
  }

  @Test
  public void test5()  throws Throwable  {
      HTTPNetworkManager hTTPNetworkManager0 = HTTPNetworkManager.getSingleton();
      String string0 = hTTPNetworkManager0.getTest503();
      assertNotNull(string0);
      assertEquals("HTTP/1.1 503 Service Unavailable\r\nConnection: Close\r\nContent-Length: 4\r\n\r\n1234", string0);
  }

  @Test
  public void test6()  throws Throwable  {
      HTTPNetworkManager hTTPNetworkManager0 = HTTPNetworkManager.getSingleton();
      assertNotNull(hTTPNetworkManager0);
      
      InetSocketAddress inetSocketAddress0 = new InetSocketAddress(0);
      Inet4Address inet4Address0 = (Inet4Address)inetSocketAddress0.getAddress();
      boolean boolean0 = hTTPNetworkManager0.isEffectiveBindAddress((InetAddress) inet4Address0);
      assertEquals(true, boolean0);
  }

  @Test
  public void test7()  throws Throwable  {
      HTTPNetworkManager hTTPNetworkManager0 = HTTPNetworkManager.getSingleton();
      boolean boolean0 = hTTPNetworkManager0.isHTTPListenerEnabled();
      assertEquals(false, boolean0);
  }

  @Test
  public void test8()  throws Throwable  {
      HTTPNetworkManager hTTPNetworkManager0 = HTTPNetworkManager.getSingleton();
      String string0 = hTTPNetworkManager0.getPingPage("HTTP/1.1 200 OK\r\nConnection: Close\r\nContent-Length: 0\r\n\r\n");
      assertEquals("HTTP/1.1 404 Not Found\r\nConnection: Close\r\nContent-Length: 0\r\n\r\n", string0);
      assertNotNull(string0);
  }

  @Test
  public void test9()  throws Throwable  {
      HTTPNetworkManager hTTPNetworkManager0 = HTTPNetworkManager.getSingleton();
      String string0 = hTTPNetworkManager0.getPingPage("?Qf011~,y");
      assertEquals("HTTP/1.1 404 Not Found\r\nConnection: Close\r\nContent-Length: 0\r\n\r\n", string0);
      assertNotNull(string0);
  }

  @Test
  public void test10()  throws Throwable  {
      HTTPNetworkManager hTTPNetworkManager0 = HTTPNetworkManager.getSingleton();
      // Undeclared exception!
      try {
        hTTPNetworkManager0.writeReply((NetworkConnection) null, (TransportHelper) null, "0acl O");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }
}
