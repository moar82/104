/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.networkmanager.impl.tcp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.networkmanager.VirtualChannelSelector;
import com.aelitis.azureus.core.networkmanager.impl.tcp.VirtualChannelSelectorImpl;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.evosuite.Properties.SandboxMode;
import org.evosuite.sandbox.Sandbox;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class VirtualChannelSelectorImplEvoSuiteTest {

  private static ExecutorService executor; 

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
    org.evosuite.Properties.SANDBOX_MODE = SandboxMode.RECOMMENDED; 
    Sandbox.initializeSecurityManagerForSUT(); 
    executor = Executors.newCachedThreadPool(); 
  } 

  @AfterClass 
  public static void clearEvoSuiteFramework(){ 
    executor.shutdownNow(); 
    Sandbox.resetDefaultSecurityManager(); 
  } 

  @Before 
  public void initTestCase(){ 
    Sandbox.goingToExecuteSUTCode(); 
  } 

  @After 
  public void doneWithTestCase(){ 
    Sandbox.doneWithExecutingSUTCode(); 
  } 


  @Test
  public void test0()  throws Throwable  {
    Future<?> future = executor.submit(new Runnable(){ 
            public void run() { 
          VirtualChannelSelector virtualChannelSelector0 = new VirtualChannelSelector("", (-1), false);
          VirtualChannelSelectorImpl virtualChannelSelectorImpl0 = new VirtualChannelSelectorImpl(virtualChannelSelector0, 7, false, true);
          int int0 = virtualChannelSelectorImpl0.select((-464L));
          assertEquals(0, int0);
      } 
    }); 
    future.get(6000, TimeUnit.MILLISECONDS); 
  }

  @Test
  public void test1()  throws Throwable  {
    Future<?> future = executor.submit(new Runnable(){ 
            public void run() { 
          VirtualChannelSelector virtualChannelSelector0 = new VirtualChannelSelector("DiabloJava HotSpot(TM) 64-Bit Server VM", (-1760), false);
          VirtualChannelSelectorImpl virtualChannelSelectorImpl0 = new VirtualChannelSelectorImpl(virtualChannelSelector0, (-1760), false, false);
          virtualChannelSelectorImpl0.setRandomiseKeys(false);
      } 
    }); 
    future.get(6000, TimeUnit.MILLISECONDS); 
  }

  @Test
  public void test2()  throws Throwable  {
    Future<?> future = executor.submit(new Runnable(){ 
            public void run() { 
          VirtualChannelSelector virtualChannelSelector0 = new VirtualChannelSelector("xCMX7n!e*5 ;*", 1, false);
          VirtualChannelSelectorImpl virtualChannelSelectorImpl0 = new VirtualChannelSelectorImpl(virtualChannelSelector0, 1, false, false);
          int int0 = virtualChannelSelectorImpl0.select((long) 1);
          assertEquals(0, int0);
      } 
    }); 
    future.get(6000, TimeUnit.MILLISECONDS); 
  }

  @Test
  public void test3()  throws Throwable  {
    Future<?> future = executor.submit(new Runnable(){ 
            public void run() { 
          VirtualChannelSelector virtualChannelSelector0 = new VirtualChannelSelector("", (-1), false);
          VirtualChannelSelectorImpl virtualChannelSelectorImpl0 = new VirtualChannelSelectorImpl(virtualChannelSelector0, (-1849), false, false);
          int int0 = virtualChannelSelectorImpl0.select((-464L));
          assertEquals(0, int0);
      } 
    }); 
    future.get(6000, TimeUnit.MILLISECONDS); 
  }

  @Test
  public void test4()  throws Throwable  {
    Future<?> future = executor.submit(new Runnable(){ 
            public void run() { 
          VirtualChannelSelector virtualChannelSelector0 = new VirtualChannelSelector("", (-1), false);
          VirtualChannelSelectorImpl virtualChannelSelectorImpl0 = new VirtualChannelSelectorImpl(virtualChannelSelector0, 7, false, true);
          virtualChannelSelectorImpl0.closeExistingSelector();
      } 
    }); 
    future.get(6000, TimeUnit.MILLISECONDS); 
  }
}
