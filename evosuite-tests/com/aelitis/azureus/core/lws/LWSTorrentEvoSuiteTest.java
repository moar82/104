/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.lws;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.activities.VuzeActivitiesEntry;
import com.aelitis.azureus.core.lws.LWSTorrent;
import com.aelitis.azureus.core.lws.LightWeightSeed;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.gudy.azureus2.core3.torrent.TOTorrent;
import org.gudy.azureus2.core3.torrent.TOTorrentAnnounceURLGroup;
import org.gudy.azureus2.core3.torrent.TOTorrentException;
import org.gudy.azureus2.core3.torrent.TOTorrentListener;
import org.gudy.azureus2.ui.swt.views.columnsetup.ColumnTC_NameInfo;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class LWSTorrentEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getAdditionalMapProperty("G]FU8");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test1()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.isDecentralised();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test2()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.addListener((TOTorrentListener) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test3()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getRelationText();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test4()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      lWSTorrent0.setPrivate(false);
      assertEquals(true, lWSTorrent0.isCreated());
  }

  @Test
  public void test5()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getPieces();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test6()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      ColumnTC_NameInfo columnTC_NameInfo0 = new ColumnTC_NameInfo((String) null);
      List<Object> list0 = columnTC_NameInfo0.getCellAddedListeners();
      // Undeclared exception!
      try {
        lWSTorrent0.setAdditionalListProperty((String) null, (List) list0);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test7()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.hasSameHashAs((TOTorrent) lWSTorrent0);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test8()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.setCreationDate(1L);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test9()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.setAdditionalStringProperty("Alg.Alias.Signature.SHA1WITHRSAENCRYPTION", "Alg.Alias.Signature.SHA1WITHRSAENCRYPTION");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test10()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.removeListener((TOTorrentListener) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test11()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      Object[] objectArray0 = lWSTorrent0.getQueryableInterfaces();
      assertNotNull(objectArray0);
  }

  @Test
  public void test12()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.setAdditionalLongProperty("/'<' i'%O", (Long) 1506L);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test13()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getSize();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test14()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      TOTorrentAnnounceURLGroup tOTorrentAnnounceURLGroup0 = lWSTorrent0.getAnnounceURLGroup();
      assertNotNull(tOTorrentAnnounceURLGroup0);
  }

  @Test
  public void test15()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getUTF8Name();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test16()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getAnnounceURL();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test17()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.removeAdditionalProperty("H6u&SI8");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test18()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getPieceLength();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test19()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      byte[] byteArray0 = new byte[5];
      // Undeclared exception!
      try {
        lWSTorrent0.setAdditionalByteArrayProperty("ScrapeInfoView", byteArray0);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test20()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getFiles();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test21()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.isSimpleTorrent();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test22()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getAdditionalLongProperty("");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test23()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getComment();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test24()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      byte[][] byteArray0 = new byte[1][3];
      // Undeclared exception!
      try {
        lWSTorrent0.setPieces(byteArray0);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test25()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getMonitor();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test26()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      boolean boolean0 = lWSTorrent0.getPrivate();
      assertEquals(false, boolean0);
  }

  @Test
  public void test27()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getAdditionalByteArrayProperty("x");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test28()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.setComment("url-list");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test29()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.print();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test30()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.serialiseToMap();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test31()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getCreatedBy();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test32()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      boolean boolean0 = lWSTorrent0.setAnnounceURL((URL) null);
      assertEquals(false, boolean0);
  }

  @Test
  public void test33()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      boolean boolean0 = lWSTorrent0.isCreated();
      assertEquals(true, boolean0);
  }

  @Test
  public void test34()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.serialiseToBEncodedFile((File) null);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test35()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getNumberOfPieces();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test36()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.removeAdditionalProperties();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test37()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      byte[] byteArray0 = new byte[4];
      // Undeclared exception!
      try {
        lWSTorrent0.setCreatedBy(byteArray0);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test38()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getAdditionalStringProperty("L6");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test39()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getCreationDate();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test40()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      VuzeActivitiesEntry vuzeActivitiesEntry0 = new VuzeActivitiesEntry(975L, (String) null, (String) null, (String) null, (String) null, (String) null);
      Map<String, Object> map0 = vuzeActivitiesEntry0.toDeletedMap();
      // Undeclared exception!
      try {
        lWSTorrent0.setAdditionalMapProperty((String) null, (Map) map0);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test41()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.setAdditionalProperty("Duplicate peer id detcted id=", "RelativePath");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test42()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getHashWrapper();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test43()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getAdditionalListProperty("");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test44()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      File file0 = new File("I]@@K4", "I]@@K4");
      // Undeclared exception!
      try {
        lWSTorrent0.serialiseToXMLFile(file0);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test45()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      byte[] byteArray0 = new byte[3];
      try {
        lWSTorrent0.setHashOverride(byteArray0);
        fail("Expecting exception: TOTorrentException");
      } catch(TOTorrentException e) {
        /*
         * Not supported
         */
      }
  }

  @Test
  public void test46()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      Object object0 = lWSTorrent0.getAdditionalProperty("url-list");
      assertNull(object0);
  }

  @Test
  public void test47()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      // Undeclared exception!
      try {
        lWSTorrent0.getAdditionalProperty("G]Fp");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test48()  throws Throwable  {
      LWSTorrent lWSTorrent0 = new LWSTorrent((LightWeightSeed) null);
      Object object0 = lWSTorrent0.getAdditionalProperty("httpseeds");
      assertNull(object0);
  }
}
