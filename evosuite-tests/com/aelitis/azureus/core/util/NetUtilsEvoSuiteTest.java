/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.util.NetUtils;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.List;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class NetUtilsEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      NetUtils netUtils0 = new NetUtils();
      assertNotNull(netUtils0);
  }

  @Test
  public void test1()  throws Throwable  {
      List<NetworkInterface> list0 = NetUtils.getNetworkInterfaces();
      assertEquals(false, list0.isEmpty());
  }
}
