/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.util.loopcontrol.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.util.loopcontrol.impl.PIDLoopControler;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class PIDLoopControlerEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      PIDLoopControler pIDLoopControler0 = new PIDLoopControler((-1.0), 1713.0718877072056, 1713.0718877072056);
      pIDLoopControler0.reset();
      assertEquals(5000.0, pIDLoopControler0.iMax, 0.01D);
      assertEquals(1713.0718877072056, pIDLoopControler0.iGain, 0.01D);
      assertEquals(1713.0718877072056, pIDLoopControler0.dGain, 0.01D);
      assertEquals(0.0, pIDLoopControler0.dState, 0.01D);
      assertEquals((-5000.0), pIDLoopControler0.iMin, 0.01D);
      assertEquals((-1.0), pIDLoopControler0.pGain, 0.01D);
      assertEquals(0.0, pIDLoopControler0.iState, 0.01D);
  }

  @Test
  public void test1()  throws Throwable  {
      PIDLoopControler pIDLoopControler0 = new PIDLoopControler((-1.0), 1713.0718877072056, 1713.0718877072056);
      double double0 = pIDLoopControler0.updateControler(9.661277687889908E9, (-935.2433381186038));
      assertEquals(5000.0, pIDLoopControler0.iState, 0.01D);
      assertEquals((-9.654314467522068E9), double0, 0.01D);
  }

  @Test
  public void test2()  throws Throwable  {
      PIDLoopControler pIDLoopControler0 = new PIDLoopControler((-1.0), 1713.0718877072056, 1713.0718877072056);
      pIDLoopControler0.iMin = 5639739.621722799;
      double double0 = pIDLoopControler0.updateControler((-1.0), (-1.0));
      assertEquals(5639739.621722799, pIDLoopControler0.iState, 0.01D);
      assertEquals(9.661277687889908E9, double0, 0.01D);
  }
}
