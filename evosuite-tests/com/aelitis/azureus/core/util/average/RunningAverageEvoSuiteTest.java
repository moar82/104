/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.util.average;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.util.average.RunningAverage;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class RunningAverageEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      RunningAverage runningAverage0 = new RunningAverage();
      runningAverage0.reset();
      assertEquals(0.0, runningAverage0.getAverage(), 0.01D);
  }

  @Test
  public void test1()  throws Throwable  {
      RunningAverage runningAverage0 = new RunningAverage();
      runningAverage0.update((-1444.9206778490184));
      double double0 = runningAverage0.getAverage();
      assertEquals((-1444.9206778490184), runningAverage0.getAverage(), 0.01D);
      assertEquals((-1444.9206778490184), double0, 0.01D);
  }

  @Test
  public void test2()  throws Throwable  {
      RunningAverage runningAverage0 = new RunningAverage();
      double double0 = runningAverage0.getAverage();
      assertEquals(0.0, double0, 0.01D);
  }
}
