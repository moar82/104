/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.peermanager.piecepicker;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.peermanager.piecepicker.EndGameModeChunk;
import org.gudy.azureus2.core3.disk.DiskManagerPiece;
import org.gudy.azureus2.core3.disk.impl.DiskManagerHelper;
import org.gudy.azureus2.core3.disk.impl.DiskManagerImpl;
import org.gudy.azureus2.core3.disk.impl.DiskManagerPieceImpl;
import org.gudy.azureus2.core3.download.DownloadManager;
import org.gudy.azureus2.core3.peer.PEPeerManager;
import org.gudy.azureus2.core3.peer.PEPiece;
import org.gudy.azureus2.core3.peer.impl.PEPieceImpl;
import org.gudy.azureus2.core3.torrent.TOTorrent;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class EndGameModeChunkEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      DiskManagerImpl diskManagerImpl0 = new DiskManagerImpl((TOTorrent) null, (DownloadManager) null);
      DiskManagerPieceImpl diskManagerPieceImpl0 = new DiskManagerPieceImpl((DiskManagerHelper) diskManagerImpl0, 0, 16);
      PEPieceImpl pEPieceImpl0 = new PEPieceImpl((PEPeerManager) null, (DiskManagerPiece) diskManagerPieceImpl0, 1);
      EndGameModeChunk endGameModeChunk0 = new EndGameModeChunk((PEPiece) pEPieceImpl0, (-816));
      int int0 = endGameModeChunk0.getOffset();
      assertEquals(16384, endGameModeChunk0.getLength());
      assertEquals((-13369344), int0);
  }

  @Test
  public void test1()  throws Throwable  {
      DiskManagerImpl diskManagerImpl0 = new DiskManagerImpl((TOTorrent) null, (DownloadManager) null);
      DiskManagerPieceImpl diskManagerPieceImpl0 = new DiskManagerPieceImpl((DiskManagerHelper) diskManagerImpl0, 0, 0);
      PEPieceImpl pEPieceImpl0 = new PEPieceImpl((PEPeerManager) null, (DiskManagerPiece) diskManagerPieceImpl0, 0);
      EndGameModeChunk endGameModeChunk0 = new EndGameModeChunk((PEPiece) pEPieceImpl0, 17170437);
      int int0 = endGameModeChunk0.getPieceNumber();
      assertEquals(0, int0);
      assertEquals(16384, endGameModeChunk0.getLength());
      assertEquals(-2147401728, endGameModeChunk0.getOffset());
  }

  @Test
  public void test2()  throws Throwable  {
      DiskManagerImpl diskManagerImpl0 = new DiskManagerImpl((TOTorrent) null, (DownloadManager) null);
      DiskManagerPieceImpl diskManagerPieceImpl0 = new DiskManagerPieceImpl((DiskManagerHelper) diskManagerImpl0, 0, 0);
      PEPieceImpl pEPieceImpl0 = new PEPieceImpl((PEPeerManager) null, (DiskManagerPiece) diskManagerPieceImpl0, 0);
      EndGameModeChunk endGameModeChunk0 = new EndGameModeChunk((PEPiece) pEPieceImpl0, 17170437);
      int int0 = endGameModeChunk0.getLength();
      assertEquals(-2147401728, endGameModeChunk0.getOffset());
      assertEquals(16384, int0);
  }

  @Test
  public void test3()  throws Throwable  {
      DiskManagerImpl diskManagerImpl0 = new DiskManagerImpl((TOTorrent) null, (DownloadManager) null);
      DiskManagerPieceImpl diskManagerPieceImpl0 = new DiskManagerPieceImpl((DiskManagerHelper) diskManagerImpl0, 0, 16);
      PEPieceImpl pEPieceImpl0 = new PEPieceImpl((PEPeerManager) null, (DiskManagerPiece) diskManagerPieceImpl0, 1);
      EndGameModeChunk endGameModeChunk0 = new EndGameModeChunk((PEPiece) pEPieceImpl0, (-816));
      int int0 = endGameModeChunk0.getBlockNumber();
      assertEquals((-816), int0);
      assertEquals(-13369344, endGameModeChunk0.getOffset());
      assertEquals(16384, endGameModeChunk0.getLength());
  }

  @Test
  public void test4()  throws Throwable  {
      DiskManagerImpl diskManagerImpl0 = new DiskManagerImpl((TOTorrent) null, (DownloadManager) null);
      DiskManagerPieceImpl diskManagerPieceImpl0 = new DiskManagerPieceImpl((DiskManagerHelper) diskManagerImpl0, 0, 16);
      PEPieceImpl pEPieceImpl0 = new PEPieceImpl((PEPeerManager) null, (DiskManagerPiece) diskManagerPieceImpl0, 1);
      EndGameModeChunk endGameModeChunk0 = new EndGameModeChunk((PEPiece) pEPieceImpl0, (-816));
      boolean boolean0 = endGameModeChunk0.equals(94, 0);
      assertEquals(false, boolean0);
      assertEquals(16384, endGameModeChunk0.getLength());
      assertEquals(-13369344, endGameModeChunk0.getOffset());
  }

  @Test
  public void test5()  throws Throwable  {
      DiskManagerImpl diskManagerImpl0 = new DiskManagerImpl((TOTorrent) null, (DownloadManager) null);
      DiskManagerPieceImpl diskManagerPieceImpl0 = new DiskManagerPieceImpl((DiskManagerHelper) diskManagerImpl0, 0, 0);
      PEPieceImpl pEPieceImpl0 = new PEPieceImpl((PEPeerManager) null, (DiskManagerPiece) diskManagerPieceImpl0, 0);
      EndGameModeChunk endGameModeChunk0 = new EndGameModeChunk((PEPiece) pEPieceImpl0, 17170437);
      boolean boolean0 = endGameModeChunk0.equals(0, (-1822));
      assertEquals(-2147401728, endGameModeChunk0.getOffset());
      assertEquals(16384, endGameModeChunk0.getLength());
      assertEquals(false, boolean0);
  }

  @Test
  public void test6()  throws Throwable  {
      DiskManagerImpl diskManagerImpl0 = new DiskManagerImpl((TOTorrent) null, (DownloadManager) null);
      DiskManagerPieceImpl diskManagerPieceImpl0 = new DiskManagerPieceImpl((DiskManagerHelper) diskManagerImpl0, 0, 0);
      PEPieceImpl pEPieceImpl0 = new PEPieceImpl((PEPeerManager) null, (DiskManagerPiece) diskManagerPieceImpl0, 0);
      EndGameModeChunk endGameModeChunk0 = new EndGameModeChunk((PEPiece) pEPieceImpl0, 0);
      boolean boolean0 = endGameModeChunk0.equals(0, 0);
      assertEquals(true, boolean0);
      assertEquals(16384, endGameModeChunk0.getLength());
  }
}
