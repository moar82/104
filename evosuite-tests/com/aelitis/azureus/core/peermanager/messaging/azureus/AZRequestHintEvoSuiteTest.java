/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.peermanager.messaging.azureus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.peermanager.messaging.MessageException;
import com.aelitis.azureus.core.peermanager.messaging.azureus.AZRequestHint;
import org.gudy.azureus2.core3.util.DirectByteBuffer;
import org.gudy.azureus2.core3.util.DirectByteBufferPool;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class AZRequestHintEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint((-1), (-1), (-1), (-117), (byte) (-63));
      String string0 = aZRequestHint0.getDescription();
      assertNotNull(string0);
      assertEquals("AZ_REQUEST_HINT piece #-1:-1->-3/-117", string0);
      assertEquals(-63, aZRequestHint0.getVersion());
  }

  @Test
  public void test1()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint((-356), (-356), 9, (-356), (byte)0);
      byte[] byteArray0 = aZRequestHint0.getIDBytes();
      assertEquals(0, aZRequestHint0.getVersion());
      assertNotNull(byteArray0);
      assertEquals("AZ_REQUEST_HINT piece #-356:-356->-348/-356", aZRequestHint0.getDescription());
  }

  @Test
  public void test2()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint((-1), (-1), (-1), (-117), (byte) (-63));
      int int0 = aZRequestHint0.getType();
      assertEquals(-63, aZRequestHint0.getVersion());
      assertEquals(0, int0);
      assertEquals("AZ_REQUEST_HINT piece #-1:-1->-3/-117", aZRequestHint0.getDescription());
  }

  @Test
  public void test3()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint(15, (-405), 1527, 15, (byte)1);
      int int0 = aZRequestHint0.getLife();
      assertEquals(1, aZRequestHint0.getVersion());
      assertEquals(15, int0);
      assertEquals("AZ_REQUEST_HINT piece #15:-405->1121/15", aZRequestHint0.getDescription());
  }

  @Test
  public void test4()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint(0, 0, 0, (-1), (byte) (-1));
      DirectByteBuffer directByteBuffer0 = DirectByteBufferPool.getBuffer((byte) (-36), 1515);
      try {
        aZRequestHint0.deserialize(directByteBuffer0, (byte)6);
        fail("Expecting exception: MessageException");
      } catch(MessageException e) {
        /*
         * [AZ_REQUEST_HINT] payload stream b-decode error: BDecoder: unknown command '0, remainder = \u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000
         */
      }
  }

  @Test
  public void test5()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint(15, (-405), 1527, 15, (byte)1);
      int int0 = aZRequestHint0.getLength();
      assertEquals("AZ_REQUEST_HINT piece #15:-405->1121/15", aZRequestHint0.getDescription());
      assertEquals(1, aZRequestHint0.getVersion());
      assertEquals(1527, int0);
  }

  @Test
  public void test6()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint(1983, 0, 0, 0, (byte)115);
      String string0 = aZRequestHint0.getFeatureID();
      assertEquals(115, aZRequestHint0.getVersion());
      assertEquals("AZ1", string0);
      assertEquals("AZ_REQUEST_HINT piece #1983:0->-1/0", aZRequestHint0.getDescription());
  }

  @Test
  public void test7()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint(15, (-405), 1527, 15, (byte)1);
      byte byte0 = aZRequestHint0.getVersion();
      assertEquals((byte)1, byte0);
      assertEquals("AZ_REQUEST_HINT piece #15:-405->1121/15", aZRequestHint0.getDescription());
  }

  @Test
  public void test8()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint((-1838), (int) (byte) (-82), 1, 64, (byte) (-82));
      int int0 = aZRequestHint0.getOffset();
      assertEquals(-82, aZRequestHint0.getVersion());
      assertEquals((-82), int0);
      assertEquals("AZ_REQUEST_HINT piece #-1838:-82->-82/64", aZRequestHint0.getDescription());
  }

  @Test
  public void test9()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint((-356), (-356), 9, (-356), (byte)0);
      int int0 = aZRequestHint0.getPieceNumber();
      assertEquals((-356), int0);
      assertEquals(0, aZRequestHint0.getVersion());
      assertEquals("AZ_REQUEST_HINT piece #-356:-356->-348/-356", aZRequestHint0.getDescription());
  }

  @Test
  public void test10()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint((-302), (-302), (-302), 190, (byte) (-111));
      int int0 = aZRequestHint0.getFeatureSubID();
      assertEquals(3, int0);
      assertEquals(-111, aZRequestHint0.getVersion());
      assertEquals("AZ_REQUEST_HINT piece #-302:-302->-605/190", aZRequestHint0.getDescription());
  }

  @Test
  public void test11()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint(1983, 0, 0, 0, (byte)115);
      DirectByteBuffer[] directByteBufferArray0 = aZRequestHint0.getData();
      assertNotNull(directByteBufferArray0);
      
      aZRequestHint0.getData();
      assertEquals(115, aZRequestHint0.getVersion());
      assertEquals("AZ_REQUEST_HINT piece #1983:0->-1/0", aZRequestHint0.getDescription());
  }

  @Test
  public void test12()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint((-1838), (int) (byte) (-82), 1, 64, (byte) (-82));
      aZRequestHint0.destroy();
      assertEquals(-82, aZRequestHint0.getVersion());
      assertEquals("AZ_REQUEST_HINT piece #-1838:-82->-82/64", aZRequestHint0.getDescription());
      assertEquals(1, aZRequestHint0.getLength());
  }

  @Test
  public void test13()  throws Throwable  {
      AZRequestHint aZRequestHint0 = new AZRequestHint(1983, 0, 0, 0, (byte)115);
      DirectByteBuffer[] directByteBufferArray0 = aZRequestHint0.getData();
      assertNotNull(directByteBufferArray0);
      
      aZRequestHint0.destroy();
      assertEquals("AZ_REQUEST_HINT piece #1983:0->-1/0", aZRequestHint0.getDescription());
      assertEquals(115, aZRequestHint0.getVersion());
  }
}
