/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.peermanager.messaging.bittorrent;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.peermanager.messaging.MessageException;
import com.aelitis.azureus.core.peermanager.messaging.bittorrent.BTDHTPort;
import org.gudy.azureus2.core3.util.DirectByteBuffer;
import org.gudy.azureus2.core3.util.DirectByteBufferPool;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class BTDHTPortEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort(1125);
      byte byte0 = bTDHTPort0.getVersion();
      assertEquals("BT_DHT_PORT (port 1125)", bTDHTPort0.getDescription());
      assertEquals((byte)1, byte0);
  }

  @Test
  public void test1()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort(1125);
      String string0 = bTDHTPort0.getFeatureID();
      assertEquals("BT1", string0);
      assertEquals("BT_DHT_PORT (port 1125)", bTDHTPort0.getDescription());
  }

  @Test
  public void test2()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort((-1873));
      int int0 = bTDHTPort0.getType();
      assertEquals(0, int0);
      assertEquals("BT_DHT_PORT (port -1873)", bTDHTPort0.getDescription());
  }

  @Test
  public void test3()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort(7);
      String string0 = bTDHTPort0.getDescription();
      assertNotNull(string0);
      assertEquals("BT_DHT_PORT (port 7)", string0);
  }

  @Test
  public void test4()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort(7);
      byte[] byteArray0 = bTDHTPort0.getIDBytes();
      assertEquals(7, bTDHTPort0.getDHTPort());
      assertNotNull(byteArray0);
      assertEquals("BT_DHT_PORT (port 7)", bTDHTPort0.getDescription());
  }

  @Test
  public void test5()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort(1125);
      int int0 = bTDHTPort0.getFeatureSubID();
      assertEquals(1125, bTDHTPort0.getDHTPort());
      assertEquals(9, int0);
  }

  @Test
  public void test6()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort(1125);
      int int0 = bTDHTPort0.getDHTPort();
      assertEquals(1125, int0);
  }

  @Test
  public void test7()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort(1125);
      DirectByteBuffer directByteBuffer0 = DirectByteBufferPool.getBuffer((byte)1, 1125);
      try {
        bTDHTPort0.deserialize(directByteBuffer0, (byte)1);
        fail("Expecting exception: MessageException");
      } catch(MessageException e) {
        /*
         * [BT_DHT_PORT] decode error: payload.remaining[1125] != 2
         */
      }
  }

  @Test
  public void test8()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort((-1));
      try {
        bTDHTPort0.deserialize((DirectByteBuffer) null, (byte)103);
        fail("Expecting exception: MessageException");
      } catch(MessageException e) {
        /*
         * [BT_DHT_PORT] decode error: data == null
         */
      }
  }

  @Test
  public void test9()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort(870);
      DirectByteBuffer directByteBuffer0 = DirectByteBufferPool.getBuffer((byte)16, 2);
      BTDHTPort bTDHTPort1 = (BTDHTPort)bTDHTPort0.deserialize(directByteBuffer0, (byte)16);
      assertEquals("BT_DHT_PORT (port 870)", bTDHTPort0.getDescription());
      assertEquals(0, bTDHTPort1.getDHTPort());
      assertNotNull(bTDHTPort1);
  }

  @Test
  public void test10()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort(7);
      DirectByteBuffer[] directByteBufferArray0 = bTDHTPort0.getData();
      assertNotNull(directByteBufferArray0);
      
      bTDHTPort0.getData();
      assertEquals(7, bTDHTPort0.getDHTPort());
      assertEquals("BT_DHT_PORT (port 7)", bTDHTPort0.getDescription());
  }

  @Test
  public void test11()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort((-1873));
      bTDHTPort0.destroy();
      assertEquals("BT_DHT_PORT (port -1873)", bTDHTPort0.getDescription());
      assertEquals(-1873, bTDHTPort0.getDHTPort());
  }

  @Test
  public void test12()  throws Throwable  {
      BTDHTPort bTDHTPort0 = new BTDHTPort(7);
      DirectByteBuffer[] directByteBufferArray0 = bTDHTPort0.getData();
      assertNotNull(directByteBufferArray0);
      
      bTDHTPort0.destroy();
      assertEquals("BT_DHT_PORT (port 7)", bTDHTPort0.getDescription());
      assertEquals(7, bTDHTPort0.getDHTPort());
  }
}
