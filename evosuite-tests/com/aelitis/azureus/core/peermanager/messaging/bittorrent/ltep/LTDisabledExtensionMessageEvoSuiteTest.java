/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.peermanager.messaging.bittorrent.ltep;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.peermanager.messaging.bittorrent.ltep.LTDisabledExtensionMessage;
import org.gudy.azureus2.core3.util.DirectByteBuffer;
import org.gudy.azureus2.core3.util.DirectByteBufferPool;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class LTDisabledExtensionMessageEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      LTDisabledExtensionMessage lTDisabledExtensionMessage0 = LTDisabledExtensionMessage.INSTANCE;
      int int0 = lTDisabledExtensionMessage0.getFeatureSubID();
      assertEquals(2, int0);
  }

  @Test
  public void test1()  throws Throwable  {
      LTDisabledExtensionMessage lTDisabledExtensionMessage0 = LTDisabledExtensionMessage.INSTANCE;
      DirectByteBuffer directByteBuffer0 = DirectByteBufferPool.getBuffer((byte)95, (byte)95);
      LTDisabledExtensionMessage lTDisabledExtensionMessage1 = (LTDisabledExtensionMessage)lTDisabledExtensionMessage0.deserialize(directByteBuffer0, (byte)95);
      assertSame(lTDisabledExtensionMessage0, lTDisabledExtensionMessage1);
  }

  @Test
  public void test2()  throws Throwable  {
      LTDisabledExtensionMessage lTDisabledExtensionMessage0 = LTDisabledExtensionMessage.INSTANCE;
      String string0 = lTDisabledExtensionMessage0.getFeatureID();
      assertEquals("LT1", string0);
  }

  @Test
  public void test3()  throws Throwable  {
      LTDisabledExtensionMessage lTDisabledExtensionMessage0 = LTDisabledExtensionMessage.INSTANCE;
      String string0 = lTDisabledExtensionMessage0.getID();
      assertEquals("disabled_extension", string0);
  }

  @Test
  public void test4()  throws Throwable  {
      LTDisabledExtensionMessage lTDisabledExtensionMessage0 = LTDisabledExtensionMessage.INSTANCE;
      lTDisabledExtensionMessage0.destroy();
      assertEquals(2, lTDisabledExtensionMessage0.getFeatureSubID());
  }

  @Test
  public void test5()  throws Throwable  {
      LTDisabledExtensionMessage lTDisabledExtensionMessage0 = LTDisabledExtensionMessage.INSTANCE;
      String string0 = lTDisabledExtensionMessage0.getDescription();
      assertEquals("Disabled extension message over LTEP (ignored)", string0);
  }

  @Test
  public void test6()  throws Throwable  {
      LTDisabledExtensionMessage lTDisabledExtensionMessage0 = LTDisabledExtensionMessage.INSTANCE;
      int int0 = lTDisabledExtensionMessage0.getType();
      assertEquals(0, int0);
  }

  @Test
  public void test7()  throws Throwable  {
      LTDisabledExtensionMessage lTDisabledExtensionMessage0 = LTDisabledExtensionMessage.INSTANCE;
      // Undeclared exception!
      try {
        lTDisabledExtensionMessage0.getData();
        fail("Expecting exception: RuntimeException");
      } catch(RuntimeException e) {
        /*
         * Disabled extension message not meant to be used for serialisation!
         */
      }
  }

  @Test
  public void test8()  throws Throwable  {
      LTDisabledExtensionMessage lTDisabledExtensionMessage0 = LTDisabledExtensionMessage.INSTANCE;
      byte byte0 = lTDisabledExtensionMessage0.getVersion();
      assertEquals((byte)0, byte0);
  }

  @Test
  public void test9()  throws Throwable  {
      LTDisabledExtensionMessage lTDisabledExtensionMessage0 = LTDisabledExtensionMessage.INSTANCE;
      byte[] byteArray0 = lTDisabledExtensionMessage0.getIDBytes();
      assertNotNull(byteArray0);
  }
}
