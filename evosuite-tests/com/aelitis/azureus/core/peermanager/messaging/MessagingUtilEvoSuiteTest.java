/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.peermanager.messaging;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.peermanager.messaging.MessageException;
import com.aelitis.azureus.core.peermanager.messaging.MessagingUtil;
import java.util.HashMap;
import java.util.Map;
import org.gudy.azureus2.core3.util.DirectByteBuffer;
import org.gudy.azureus2.core3.util.DirectByteBufferPool;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class MessagingUtilEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      HashMap<Integer, Object> hashMap0 = new HashMap<Integer, Object>();
      DirectByteBuffer directByteBuffer0 = MessagingUtil.convertPayloadToBencodedByteStream((Map) hashMap0, (byte)0);
      try {
        MessagingUtil.convertBencodedByteStreamToPayload(directByteBuffer0, 22, "o%~;k)m");
        fail("Expecting exception: MessageException");
      } catch(MessageException e) {
        /*
         * [o%~;k)m] decode error: stream.remaining[2] < 22
         */
      }
  }

  @Test
  public void test1()  throws Throwable  {
      MessagingUtil messagingUtil0 = new MessagingUtil();
      assertNotNull(messagingUtil0);
  }

  @Test
  public void test2()  throws Throwable  {
      DirectByteBuffer directByteBuffer0 = DirectByteBufferPool.getBuffer((byte)21, (byte)21);
      try {
        MessagingUtil.convertBencodedByteStreamToPayload(directByteBuffer0, (int) (byte)21, "");
        fail("Expecting exception: MessageException");
      } catch(MessageException e) {
        /*
         * [] payload stream b-decode error: BDecoder: unknown command '0, remainder = \u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000
         */
      }
  }

  @Test
  public void test3()  throws Throwable  {
      try {
        MessagingUtil.convertBencodedByteStreamToPayload((DirectByteBuffer) null, 24, "}```[*Feo3\"|JM>");
        fail("Expecting exception: MessageException");
      } catch(MessageException e) {
        /*
         * [}```[*Feo3\"|JM>] decode error: stream == null
         */
      }
  }
}
