/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.core.devices.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.devices.DeviceManagerException;
import com.aelitis.azureus.core.devices.TranscodeProfile;
import com.aelitis.azureus.core.devices.impl.DeviceManagerImpl;
import com.aelitis.azureus.core.devices.impl.DeviceMediaRendererTemplateImpl;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DeviceMediaRendererTemplateImplEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, " 6z-vfIza+X", true);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      
      String string0 = deviceMediaRendererTemplateImpl0.getManufacturer();
      assertEquals(" 6z-vfIza+X", string0);
      assertEquals(true, deviceMediaRendererTemplateImpl0.isAuto());
      assertNotNull(string0);
      assertEquals(" 6z-vfIza+X", deviceMediaRendererTemplateImpl0.getName());
  }

  @Test
  public void test1()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, "", false);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      
      TranscodeProfile[] transcodeProfileArray0 = deviceMediaRendererTemplateImpl0.getProfiles();
      assertNotNull(transcodeProfileArray0);
      assertEquals(false, deviceMediaRendererTemplateImpl0.isAuto());
  }

  @Test
  public void test2()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, " 6z-vfIza+X", true);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      
      int int0 = deviceMediaRendererTemplateImpl0.getRendererSpecies();
      assertEquals(6, int0);
      assertEquals(true, deviceMediaRendererTemplateImpl0.isAuto());
      assertEquals(" 6z-vfIza+X", deviceMediaRendererTemplateImpl0.getName());
      assertEquals(" 6z-vfIza+X", deviceMediaRendererTemplateImpl0.getManufacturer());
  }

  @Test
  public void test3()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, "aA{j|)", false);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      
      String string0 = deviceMediaRendererTemplateImpl0.getClassification();
      assertEquals(false, deviceMediaRendererTemplateImpl0.isAuto());
      assertEquals("aA{j|)", deviceMediaRendererTemplateImpl0.getManufacturer());
      assertNotNull(string0);
      assertEquals("aA{j|)", deviceMediaRendererTemplateImpl0.getName());
  }

  @Test
  public void test4()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, "|b", false);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      
      deviceMediaRendererTemplateImpl0.getShortDescription();
      assertEquals("|b", deviceMediaRendererTemplateImpl0.getName());
      assertEquals(false, deviceMediaRendererTemplateImpl0.isAuto());
      assertEquals("|b", deviceMediaRendererTemplateImpl0.getManufacturer());
  }

  @Test
  public void test5()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, "", false);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      
      String string0 = deviceMediaRendererTemplateImpl0.getName();
      assertNotNull(string0);
      assertEquals(false, deviceMediaRendererTemplateImpl0.isAuto());
  }

  @Test
  public void test6()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, "874~ydTC}*E[\"i#", false);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      
      // Undeclared exception!
      try {
        deviceMediaRendererTemplateImpl0.createInstance("874~ydTC}*E[\"i#");
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test7()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, "DiKYzp", true);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      
      int int0 = deviceMediaRendererTemplateImpl0.getType();
      assertEquals(true, deviceMediaRendererTemplateImpl0.isAuto());
      assertEquals("DiKYzp", deviceMediaRendererTemplateImpl0.getManufacturer());
      assertEquals("DiKYzp", deviceMediaRendererTemplateImpl0.getName());
      assertEquals(3, int0);
  }

  @Test
  public void test8()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, " 6z-vfIza+X", true);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      
      boolean boolean0 = deviceMediaRendererTemplateImpl0.isAuto();
      assertEquals(" 6z-vfIza+X", deviceMediaRendererTemplateImpl0.getName());
      assertEquals(" 6z-vfIza+X", deviceMediaRendererTemplateImpl0.getManufacturer());
      assertEquals(true, boolean0);
  }

  @Test
  public void test9()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, "|b", false);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      
      deviceMediaRendererTemplateImpl0.addProfile((TranscodeProfile) null);
      assertEquals("|b", deviceMediaRendererTemplateImpl0.getManufacturer());
      assertEquals(false, deviceMediaRendererTemplateImpl0.isAuto());
      assertEquals("|b", deviceMediaRendererTemplateImpl0.getName());
  }

  @Test
  public void test10()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, "GeneralView.label.uploadspeed", true);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      assertEquals("GeneralView", deviceMediaRendererTemplateImpl0.getManufacturer());
      assertEquals("uploadspeed", deviceMediaRendererTemplateImpl0.getName());
      assertEquals(true, deviceMediaRendererTemplateImpl0.isAuto());
  }

  @Test
  public void test11()  throws Throwable  {
      DeviceMediaRendererTemplateImpl deviceMediaRendererTemplateImpl0 = new DeviceMediaRendererTemplateImpl((DeviceManagerImpl) null, " 6z-vfIza+X", true);
      assertNotNull(deviceMediaRendererTemplateImpl0);
      
      try {
        deviceMediaRendererTemplateImpl0.createInstance("", "Tracker Port Force External", true);
        fail("Expecting exception: DeviceManagerException");
      } catch(DeviceManagerException e) {
        /*
         * Device can't be added manually
         */
      }
  }
}
