/*
 * This file was automatically generated by EvoSuite
 */

package com.aelitis.azureus.plugins.dht.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import com.aelitis.azureus.core.dht.transport.DHTTransportValue;
import com.aelitis.azureus.plugins.dht.impl.DHTPluginValueImpl;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DHTPluginValueImplEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      DHTPluginValueImpl dHTPluginValueImpl0 = new DHTPluginValueImpl((DHTTransportValue) null);
      // Undeclared exception!
      try {
        dHTPluginValueImpl0.getFlags();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test1()  throws Throwable  {
      DHTPluginValueImpl dHTPluginValueImpl0 = new DHTPluginValueImpl((DHTTransportValue) null);
      // Undeclared exception!
      try {
        dHTPluginValueImpl0.getValue();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test2()  throws Throwable  {
      DHTPluginValueImpl dHTPluginValueImpl0 = new DHTPluginValueImpl((DHTTransportValue) null);
      // Undeclared exception!
      try {
        dHTPluginValueImpl0.getCreationTime();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test3()  throws Throwable  {
      DHTPluginValueImpl dHTPluginValueImpl0 = new DHTPluginValueImpl((DHTTransportValue) null);
      // Undeclared exception!
      try {
        dHTPluginValueImpl0.isLocal();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test4()  throws Throwable  {
      DHTPluginValueImpl dHTPluginValueImpl0 = new DHTPluginValueImpl((DHTTransportValue) null);
      // Undeclared exception!
      try {
        dHTPluginValueImpl0.getVersion();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }
}
