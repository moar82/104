/*
 * This file was automatically generated by EvoSuite
 */

package org.bouncycastle.asn1.sec;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.io.IOException;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.DERApplicationSpecific;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERConstructedSequence;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.sec.ECPrivateKeyStructure;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class ECPrivateKeyStructureEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      BigInteger bigInteger0 = BigInteger.ZERO;
      ECPrivateKeyStructure eCPrivateKeyStructure0 = new ECPrivateKeyStructure(bigInteger0);
      assertNotNull(eCPrivateKeyStructure0);
      
      DERSequence dERSequence0 = (DERSequence)eCPrivateKeyStructure0.getDERObject();
      assertNotNull(dERSequence0);
      assertEquals("[1, #]", dERSequence0.toString());
  }

  @Test
  public void test1()  throws Throwable  {
      DERConstructedSequence dERConstructedSequence0 = new DERConstructedSequence();
      BERSequence bERSequence0 = new BERSequence((DEREncodable) dERConstructedSequence0);
      ECPrivateKeyStructure eCPrivateKeyStructure0 = new ECPrivateKeyStructure((ASN1Sequence) bERSequence0);
      // Undeclared exception!
      try {
        eCPrivateKeyStructure0.getKey();
        fail("Expecting exception: ArrayIndexOutOfBoundsException");
      } catch(ArrayIndexOutOfBoundsException e) {
        /*
         * 1 >= 1
         */
      }
  }

  @Test
  public void test2()  throws Throwable  {
      BigInteger bigInteger0 = BigInteger.ZERO;
      ECPrivateKeyStructure eCPrivateKeyStructure0 = new ECPrivateKeyStructure(bigInteger0);
      DERBitString dERBitString0 = eCPrivateKeyStructure0.getPublicKey();
      assertNull(dERBitString0);
  }

  @Test
  public void test3()  throws Throwable  {
      BigInteger bigInteger0 = BigInteger.TEN;
      ECPrivateKeyStructure eCPrivateKeyStructure0 = new ECPrivateKeyStructure(bigInteger0);
      assertNotNull(eCPrivateKeyStructure0);
  }

  @Test
  public void test4()  throws Throwable  {
      byte[] byteArray0 = new byte[5];
      byteArray0[0] = (byte)90;
      DERApplicationSpecific dERApplicationSpecific0 = (DERApplicationSpecific)ASN1Object.fromByteArray(byteArray0);
      BERTaggedObject bERTaggedObject0 = new BERTaggedObject(0, (DEREncodable) dERApplicationSpecific0);
      DERSequence dERSequence0 = new DERSequence((DEREncodable) bERTaggedObject0);
      ECPrivateKeyStructure eCPrivateKeyStructure0 = new ECPrivateKeyStructure((ASN1Sequence) dERSequence0);
      DERApplicationSpecific dERApplicationSpecific1 = (DERApplicationSpecific)eCPrivateKeyStructure0.getParameters();
      assertEquals(false, dERApplicationSpecific1.isConstructed());
  }

  @Test
  public void test5()  throws Throwable  {
      byte[] byteArray0 = new byte[5];
      byteArray0[0] = (byte)90;
      DERApplicationSpecific dERApplicationSpecific0 = (DERApplicationSpecific)ASN1Object.fromByteArray(byteArray0);
      BERTaggedObject bERTaggedObject0 = new BERTaggedObject(0, (DEREncodable) dERApplicationSpecific0);
      DERSequence dERSequence0 = new DERSequence((DEREncodable) bERTaggedObject0);
      ECPrivateKeyStructure eCPrivateKeyStructure0 = new ECPrivateKeyStructure((ASN1Sequence) dERSequence0);
      DERBitString dERBitString0 = eCPrivateKeyStructure0.getPublicKey();
      assertNull(dERBitString0);
  }
}
