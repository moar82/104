/*
 * This file was automatically generated by EvoSuite
 */

package org.bouncycastle.asn1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DERSetEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      DEREncodableVector dEREncodableVector0 = new DEREncodableVector();
      DERSet dERSet0 = new DERSet(dEREncodableVector0);
      byte[] byteArray0 = dERSet0.getEncoded();
      assertNotNull(byteArray0);
  }

  @Test
  public void test1()  throws Throwable  {
      DERSet dERSet0 = new DERSet();
      assertEquals("[]", dERSet0.toString());
  }

  @Test
  public void test2()  throws Throwable  {
      BERSet bERSet0 = new BERSet((DEREncodable) null);
      assertEquals(1, bERSet0.size());
  }

  @Test
  public void test3()  throws Throwable  {
      ASN1Encodable[] aSN1EncodableArray0 = new ASN1Encodable[1];
      DERSet dERSet0 = new DERSet(aSN1EncodableArray0);
      assertNotNull(dERSet0);
      
      byte[] byteArray0 = dERSet0.getDEREncoded();
      assertNotNull(byteArray0);
      assertEquals(1, dERSet0.size());
      assertEquals("[null]", dERSet0.toString());
  }

  @Test
  public void test4()  throws Throwable  {
      DEREncodableVector dEREncodableVector0 = new DEREncodableVector();
      dEREncodableVector0.add((DEREncodable) null);
      BERSet bERSet0 = new BERSet(dEREncodableVector0);
      assertNotNull(bERSet0);
      assertEquals("[null]", bERSet0.toString());
  }
}
