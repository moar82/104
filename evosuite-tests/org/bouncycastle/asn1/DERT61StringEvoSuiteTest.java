/*
 * This file was automatically generated by EvoSuite
 */

package org.bouncycastle.asn1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.BERConstructedOctetString;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DEREnumerated;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERT61String;
import org.bouncycastle.asn1.DERTaggedObject;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DERT61StringEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      DERT61String dERT61String0 = new DERT61String("crash");
      String string0 = dERT61String0.toString();
      assertEquals("crash", string0);
      assertNotNull(string0);
  }

  @Test
  public void test1()  throws Throwable  {
      DERT61String dERT61String0 = new DERT61String("");
      boolean boolean0 = dERT61String0.asn1Equals((DERObject) dERT61String0);
      assertEquals("", dERT61String0.getString());
      assertEquals(true, boolean0);
  }

  @Test
  public void test2()  throws Throwable  {
      byte[] byteArray0 = new byte[1];
      DEREnumerated dEREnumerated0 = new DEREnumerated(byteArray0);
      BERTaggedObject bERTaggedObject0 = new BERTaggedObject(0, (DEREncodable) dEREnumerated0);
      // Undeclared exception!
      try {
        DERT61String.getInstance((ASN1TaggedObject) bERTaggedObject0, true);
        fail("Expecting exception: IllegalArgumentException");
      } catch(IllegalArgumentException e) {
        /*
         * illegal object in getInstance: org.bouncycastle.asn1.DEREnumerated
         */
      }
  }

  @Test
  public void test3()  throws Throwable  {
      DERT61String dERT61String0 = new DERT61String("N");
      int int0 = dERT61String0.hashCode();
      assertEquals(78, int0);
  }

  @Test
  public void test4()  throws Throwable  {
      DERT61String dERT61String0 = DERT61String.getInstance((Object) null);
      assertNull(dERT61String0);
  }

  @Test
  public void test5()  throws Throwable  {
      DERT61String dERT61String0 = new DERT61String("v&_*5Kw24Q:5BGPnW");
      DERT61String dERT61String1 = DERT61String.getInstance((Object) dERT61String0);
      assertNotNull(dERT61String1);
      assertEquals("v&_*5Kw24Q:5BGPnW", dERT61String0.getString());
      assertEquals("v&_*5Kw24Q:5BGPnW", dERT61String1.getString());
  }

  @Test
  public void test6()  throws Throwable  {
      DERTaggedObject dERTaggedObject0 = new DERTaggedObject(0);
      BERConstructedOctetString bERConstructedOctetString0 = (BERConstructedOctetString)ASN1OctetString.getInstance((ASN1TaggedObject) dERTaggedObject0, true);
      DERT61String dERT61String0 = DERT61String.getInstance((Object) bERConstructedOctetString0);
      assertEquals("", dERT61String0.getString());
  }

  @Test
  public void test7()  throws Throwable  {
      BERTaggedObject bERTaggedObject0 = new BERTaggedObject((-1266));
      // Undeclared exception!
      try {
        DERT61String.getInstance((Object) bERTaggedObject0);
        fail("Expecting exception: IllegalArgumentException");
      } catch(IllegalArgumentException e) {
        /*
         * illegal object in getInstance: org.bouncycastle.asn1.BERSequence
         */
      }
  }

  @Test
  public void test8()  throws Throwable  {
      byte[] byteArray0 = new byte[2];
      DERT61String dERT61String0 = new DERT61String(byteArray0);
      assertNotNull(dERT61String0);
      assertEquals("\u0000\u0000", dERT61String0.toString());
  }

  @Test
  public void test9()  throws Throwable  {
      DERT61String dERT61String0 = new DERT61String("N");
      byte[] byteArray0 = dERT61String0.getDEREncoded();
      assertEquals("N", dERT61String0.getString());
      assertNotNull(byteArray0);
      assertEquals("N", dERT61String0.toString());
  }

  @Test
  public void test10()  throws Throwable  {
      DERT61String dERT61String0 = new DERT61String("N");
      boolean boolean0 = dERT61String0.asn1Equals((DERObject) null);
      assertEquals(false, boolean0);
      assertEquals("N", dERT61String0.getString());
  }
}
