/*
 * This file was automatically generated by EvoSuite
 */

package org.bouncycastle.asn1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import java.util.Vector;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.BERConstructedOctetString;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERGeneralString;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERTaggedObject;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class DERGeneralStringEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      byte[] byteArray0 = new byte[2];
      DERGeneralString dERGeneralString0 = new DERGeneralString(byteArray0);
      assertNotNull(dERGeneralString0);
      
      int int0 = dERGeneralString0.hashCode();
      assertEquals(0, int0);
  }

  @Test
  public void test1()  throws Throwable  {
      byte[] byteArray0 = new byte[2];
      DERGeneralString dERGeneralString0 = new DERGeneralString(byteArray0);
      assertNotNull(dERGeneralString0);
      
      byte[] byteArray1 = dERGeneralString0.getDEREncoded();
      assertNotNull(byteArray1);
      assertEquals("\u0000\u0000", dERGeneralString0.toString());
      assertEquals("\u0000\u0000", dERGeneralString0.getString());
  }

  @Test
  public void test2()  throws Throwable  {
      DERGeneralString dERGeneralString0 = new DERGeneralString((String) null);
      String string0 = dERGeneralString0.toString();
      assertNull(string0);
  }

  @Test
  public void test3()  throws Throwable  {
      DERGeneralString dERGeneralString0 = new DERGeneralString("lib");
      DERTaggedObject dERTaggedObject0 = new DERTaggedObject(18, (DEREncodable) dERGeneralString0);
      DERGeneralString dERGeneralString1 = DERGeneralString.getInstance((ASN1TaggedObject) dERTaggedObject0, true);
      assertNotNull(dERGeneralString1);
      assertEquals("lib", dERGeneralString0.getString());
      assertEquals("lib", dERGeneralString1.toString());
  }

  @Test
  public void test4()  throws Throwable  {
      DERGeneralString dERGeneralString0 = DERGeneralString.getInstance((Object) null);
      assertNull(dERGeneralString0);
  }

  @Test
  public void test5()  throws Throwable  {
      // Undeclared exception!
      try {
        DERGeneralString.getInstance((Object) "zh");
        fail("Expecting exception: IllegalArgumentException");
      } catch(IllegalArgumentException e) {
        /*
         * illegal object in getInstance: java.lang.String
         */
      }
  }

  @Test
  public void test6()  throws Throwable  {
      Vector<ASN1OctetString> vector0 = new Vector<ASN1OctetString>();
      BERConstructedOctetString bERConstructedOctetString0 = new BERConstructedOctetString(vector0);
      BERTaggedObject bERTaggedObject0 = new BERTaggedObject(true, 7, (DEREncodable) bERConstructedOctetString0);
      DERGeneralString dERGeneralString0 = DERGeneralString.getInstance((Object) bERTaggedObject0);
      assertEquals("", dERGeneralString0.getString());
  }

  @Test
  public void test7()  throws Throwable  {
      DERGeneralString dERGeneralString0 = new DERGeneralString("8O");
      boolean boolean0 = dERGeneralString0.asn1Equals((DERObject) dERGeneralString0);
      assertEquals(true, boolean0);
      assertEquals("8O", dERGeneralString0.getString());
  }

  @Test
  public void test8()  throws Throwable  {
      DERGeneralString dERGeneralString0 = new DERGeneralString((String) null);
      DERTaggedObject dERTaggedObject0 = new DERTaggedObject(1453);
      boolean boolean0 = dERGeneralString0.asn1Equals((DERObject) dERTaggedObject0);
      assertEquals(false, boolean0);
  }
}
